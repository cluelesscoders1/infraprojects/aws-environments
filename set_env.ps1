$Env:AWS_ACCESS_KEY_ID="redacted"
$Env:AWS_SECRET_ACCESS_KEY="redacted"
$Env:AWS_DEFAULT_REGION="us-east-1"
$Env:AWS_REGION="us-east-1"
$Env:AWS_ROLE_ARN="redacted"
$Env:AWS_ROLE_SESSION_NAME="redacted"

function impersonate{
    $credentials=aws sts assume-role --role-arn $Env:AWS_ROLE_ARN --role-session-name $Env:AWS_ROLE_SESSION_NAME --query 'Credentials.{AKI:AccessKeyId,SAK:SecretAccessKey,ST:SessionToken}' --output text
    $cred_list=$credentials -split " "
    $Env:AWS_ACCESS_KEY_ID=$cred_list[1]
    $Env:AWS_SECRET_ACCESS_KEY=$cred_list[2]
    $Env:AWS_SECURITY_TOKEN=$cred_list[3]
}

function clear-creds {
    Remove-Variable AWS_ACCESS_KEY_ID
    Remove-Variable AWS_SECRET_ACCESS_KEY
    Remove-Variable AWS_DEFAULT_REGION
    Remove-Variable AWS_REGION
    Remove-Variable AWS_ROLE_ARN
    Remove-Variable AWS_ROLE_SESSION_NAME
}

function testawsid() {
    aws sts get-caller-identity
}
