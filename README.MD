[[_TOC_]]

# AWS Environment

The scripts contained in this repository are specifically designed to change the AWS login and impersonate AWS accounts if desired. The scripts are similar as when sourced, will override any AWS profile settings allowing for easy switching between multiple AWS environments. 

# Bash

First written to allow and notify the user of which AWS profile is being used with an updated ps1 line. This script will load the environment with the variables set on line 6 and 7. 

## Use of Bash

To use, simple use `source <path>/set_env.sh`. You will know when the loading is successful with the change in the PS1 line. To stop using the specific environment, use `deactivate`. 

To impersonate, uncomment and set lines 9 and 10. To begin impersonating, either uncomment line 69 for impersonating on load, or simply set with:
```bash
impersonate $AWS_ROLE_ARN $AWS_ROLE_SESSION_NAME
```

To verify what user is set, you always use the `testawsid` function.  

# PowerShell

Similar to the bash script with the most basic functions. However, one must use the `clear-creds` function instead of `deactivate` in bash when you stop using or wish to switch profiles. 

## Use of PowerShell

Set the environment variables in script. 

* Loading: `. .\set_env.ps1`
* Impersonating: `impersonate`
* Deactivating: `clear-creds`

Please note, one may have clear-credentials and reload the script and impersonate again of a timeout for role assumption has timed out. 
